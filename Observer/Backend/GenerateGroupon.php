<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Observer\Backend;

use Magenest\Groupon\Model\Deal;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Item as OrderItem;

/**
 * Class GenerateGroupon
 * @package Magenest\Groupon\Observer\Backend
 */
class GenerateGroupon implements ObserverInterface
{

    /**
     * email config
     */
    const XML_PATH_EMAIL = 'groupon/email_config/email';
    /**
     * qty config
     */
    const XML_PATH_QTY = 'groupon/general_config/delete_qty';

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magenest\Groupon\Model\DealFactory
     */
    protected $deal;

    /**
     * @var \Magenest\Groupon\Model\GrouponFactory
     */
    protected $groupon;

    /**
     * @var \Magenest\Groupon\Model\TicketFactory
     */
    protected $ticket;

    /**
     * @var \Magenest\Groupon\Helper\RenderCode
     */
    protected $renderCode;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * GenerateGroupon constructor.
     * @param \Psr\Log\LoggerInterface $loggerInterface
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param \Magenest\Groupon\Model\GrouponFactory $grouponFactory
     * @param \Magenest\Groupon\Model\TicketFactory $ticketFactory
     * @param \Magenest\Groupon\Helper\RenderCode $renderCode
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magenest\Groupon\Model\GrouponFactory $grouponFactory,
        \Magenest\Groupon\Model\TicketFactory $ticketFactory,
        \Magenest\Groupon\Helper\RenderCode $renderCode,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->deal = $dealFactory;
        $this->groupon = $grouponFactory;
        $this->ticket = $ticketFactory;
        $this->_scopeConfig = $scopeConfigInterface;
        $this->logger = $loggerInterface;
        $this->renderCode = $renderCode;
        $this->messageManager = $messageManager;
    }

    /**
     * Set new customer group to all his quotes
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /** @var OrderItem $orderItem */
        $orderItem = $observer->getEvent()->getItem();
        /** @var \Magento\Catalog\Model\Product $product */
        $productType = $orderItem->getProductType();
        $buyInfo = $orderItem->getBuyRequest();
        $options = $buyInfo->getAdditionalOptions();
        $modelDeal = $this->deal->create()->load($orderItem->getProductId(), 'product_id');
        if ($modelDeal->getId() && $productType == 'coupon' && $orderItem->getStatusId() == OrderItem::STATUS_INVOICED) {
            /** @var \Magento\Sales\Model\Order $order */
            $order = $orderItem->getOrder();
            $qty = $orderItem->getQtyOrdered();
            $customerName = 'Guest';
            $customer = $order->getCustomerName();
            if (isset($customer) && !empty($customer)) {
                $customerName = $customer;
            }
            $this->setDealQty($modelDeal, $orderItem);

            $configEmail = $this->_scopeConfig->getValue(self::XML_PATH_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, 0);
            if ($configEmail == 'send_multi_email') {
                $putQty = 1;
                $number = $qty;
            } else {
                $putQty = $qty;
                $number = 1;
            }
            $locationText = null;
            if (isset($options['groupon_location_id'])) {
                $locationId = $options['groupon_location_id'];
                $locations  = unserialize($modelDeal->getLocation());
                foreach ($locations as $location) {
                    if ($location['record_id'] == $locationId) {
                        $locationText = $location['street'] . "-" . $location['city'] . "-" . $location['country'];
                    }
                }
            }
            $grouponData = [
                'product_id' => $orderItem->getProductId(),
                'product_name' => $orderItem->getName(),
                'order_id' => $order->getId(),
                'customer_id'=>$order->getCustomerId(),
                'customer_email'=> $order->getCustomerEmail(),
                'customer_name'=> $customerName,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'qty' => $putQty,
                'status' => 1,
                'location' => $locationText
            ];
            for ($i = 0; $i < $number; $i++) {
                /** @var array $ticketData */
                $grouponData['groupon_code'] = $order->getIncrementId().'-'.$orderItem->getProductId().'-'.$this->generateRandomString();
                $grouponData['redemption_code'] = $this->renderCode->generateCode();
                $modelGroupon = $this->groupon->create();
                $modelGroupon->setData($grouponData)->save();
            }

            try {
                $modelTicket = $this->groupon->create()->getCollection()
                    ->addFieldToFilter('order_id', $order->getId())
                    ->addFieldToFilter('product_id', $orderItem->getProductId());
                foreach ($modelTicket as $ticketMail) {
                    $this->groupon->create()->sendMail($ticketMail->getGrouponId());
                }
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }

        return;
    }

    /**
     * @param int $length
     * @return string
     */
    public function generateRandomString()
    {
        $length = 12;
        return substr(str_shuffle(str_repeat($x = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)))), 1, $length);
    }

    /**
     * @param Deal $deal
     * @param OrderItem $item
     */
    protected function setDealQty($deal, $item)
    {
        try {
            $dealPurchasedQty = $deal->getData('purchased_qty');
            $invoicedQty = $item->getQtyInvoiced();
            $deal->setData('purchased_qty', (int)($dealPurchasedQty ? $invoicedQty + $dealPurchasedQty : $invoicedQty));
            $deal->setData('available_qty', (int)($deal->getData('available_qty') - $invoicedQty));
            $deal->save();
        } catch (\Exception $e) {
            \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->debug('Save Deal Qty Exception: '.$e->getMessage());
        }
    }
}
