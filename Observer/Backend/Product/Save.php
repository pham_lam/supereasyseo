<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Observer\Backend\Product;

use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Filesystem;
use Psr\Log\LoggerInterface;
use Magenest\Groupon\Model\DealFactory;
use Magenest\Groupon\Model\TicketFactory;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;

/**
 * Class Save
 * @package Magenest\Groupon\Observer\Backend\Product
 */
class Save implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var Filesystem
     */
    protected $_filesystem;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManage;

    /**
     * @var DealFactory
     */
    protected $deal;

    /**
     * @var TicketFactory
     */
    protected $ticket;

    /**
     * @var Configurable
     */
    protected $configurable;

    /**
     * @var StockStateInterface
     */
    protected $stockStateInterface;

    /**
     * Save constructor.
     * @param RequestInterface $request
     * @param Filesystem $filesystem
     * @param StoreManagerInterface $storeManagerInterface
     * @param LoggerInterface $logger
     * @param DealFactory $dealFactory
     * @param OptionFactory $optionFactory
     * @param TicketFactory $ticketFactory
     * @param Configurable $configurable
     * @param StockStateInterface $stockStateInterface
     */
    public function __construct(
        RequestInterface $request,
        Filesystem $filesystem,
        StoreManagerInterface $storeManagerInterface,
        LoggerInterface $logger,
        DealFactory $dealFactory,
        TicketFactory $ticketFactory,
        Configurable $configurable,
        StockStateInterface $stockStateInterface
    ) {
        $this->configurable = $configurable;
        $this->storeManage = $storeManagerInterface;
        $this->_request = $request;
        $this->_filesystem = $filesystem;
        $this->_logger = $logger;
        $this->deal = $dealFactory;
        $this->ticket = $ticketFactory;
        $this->stockStateInterface = $stockStateInterface;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        $productId = $product->getId();
        $status = $product->getStatus();
        $productTypeId = $product->getTypeId();
        $params = $this->_request->getParams();
        $store = $product->getStore()->getStoreId();
//        $this->_logger->debug(print_r($params, true));
        if (!empty($params['groupon'])) {
            if ($productTypeId == 'configurable' || $productTypeId == 'coupon') {
                $array = $params['groupon'];
                if ($productTypeId == 'coupon') {
                    $data['product_id'] = $productId;
                    $modelConfigurable = $this->configurable->getParentIdsByChild($productId);
                    if (isset($modelConfigurable[0])) {
                        $data['parent_id'] = $modelConfigurable[0];
                    }
                    if (isset($array['location']) && !empty($array['location'])) {
                        $data['location'] = serialize($array['location']);
                    }
                }
                $data['product_type'] = $productTypeId;
                $data['enable'] = $array['enable_groupon'];
                if ($array['enable_groupon'] == 1) {
                    /** save deal */
                    $data['product_id'] = $productId;
                    $data['store_id'] = $store;
                    $data['start_time'] = $array['start_time'];
                    $data['end_time'] = $array['end_time'];
                    $data['groupon_expire'] = $array['groupon_expire'];
                    $data['reminder_before_day'] = $array['redeemable_after'];
//                $data['vendor_id'] = $array['company'];
                    $data['minimum_buyers_limit'] = $array['minimum'];
                    $data['maximum_buyers_limit'] = $array['maximum'];
                    $data['priority'] = $array['priority'];
//                    $data['user_limit'] = $array['groupon_per_user_limit'];
                    $data['status '] = $status;
                    $qtyInStock = $this->stockStateInterface->getStockQty($product->getId(), $product->getStore()->getWebsiteId());

                    if ($productTypeId == 'coupon') {
                        $data['qty'] = $qtyInStock;
                        $data['available_qty'] = $product->getData('purchased_qty') ? $qtyInStock - $product->getData('purchased_qty') : $qtyInStock ;
                    }
                    if ($array['ticket']) {
                        $this->saveTicket($array['ticket'], $productId);
                    }
                }
                $model = $this->deal->create()->load($productId, 'product_id');
                $model->addData($data)->save();
            }
        }

        return;
    }

    /**
     * @param $data
     * @param $productId
     */
    public function saveTicket($data, $productId)
    {
        $ticket['product_id'] = $productId;
        $ticket['coordinates'] = serialize([]);
        $result = [];
        if (isset($data['coordinates'])) {
            $coordinates = $data['coordinates'];
            if (isset($coordinates) && !empty($coordinates)) {
                $size = sizeof($coordinates);
                for ($i = 0; $i<$size; $i++) {
                    if (empty($coordinates[$i]['is_delete'])) {
                        $ticket['coordinates'] = $coordinates[$i];
                        $result[] = $ticket['coordinates'];
                    }
                }
                $ticket['coordinates'] = serialize($result);
                unset($coordinates);
            }
        }

        //save frontent side
        $background = [];
        if (isset($data['background'])) {
            $background = $data['background'];
        }
        $ticket['background'] = serialize([]);
        if (isset($background) && !empty($background)) {
            $ticket['background'] = serialize($background);
        }
        $ticket['page_height'] = $data['page_height'];
        $ticket['page_width'] = $data['page_width'];
        $ticket['terms'] = $data['terms'];
        $ticket['instructions'] = $data['instructions'];

        $modelTicket = $this->ticket->create()->load($productId, 'product_id');
        $modelTicket->addData($ticket)->save();
    }
}
