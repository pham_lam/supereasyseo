<?php
namespace Magenest\Groupon\Observer\Option;

use Magento\Catalog\Model\Plugin\QuoteItemProductOption;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class Cart
 * @package Magenest\Groupon\Observer\Option
 */
class Cart implements ObserverInterface
{
    protected $_logger;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $_currency;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magenest\Groupon\Model\DealFactory
     */
    protected $dealFactory;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;

    /**
     * @var ProductFactory
     */
    protected $productFactory;


    /**
     * Cart constructor.
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger,
        \Magento\Directory\Model\Currency $currency,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        ProductFactory $productFactory
    ) {
    
        $this->_productRepository = $productRepository;
        $this->dealFactory = $dealFactory;
        $this->_logger = $logger;
        $this->_currency = $currency;
        $this->_request = $request;
        $this->productFactory = $productFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            /** @var \Magento\Quote\Model\Quote\Item $item */
            $item = $observer->getEvent()->getQuoteItem();
            /** @var Product $product */
            $product = $item->getProduct();
            $productId = $product->getId();
            $data = $this->_request->getParams();
            $checkTypeProduct = $this->_productRepository->getById($productId)->getTypeId();
            if ($checkTypeProduct == 'configurable') {
                if (isset($data['additional_options'])) {
                    $additionalOptions = [];
                    if (isset($data['selected_configurable_option']) && $data['selected_configurable_option']) {
                        $productId = $data['selected_configurable_option'];
                    }
                    if (isset($data['additional_options']['groupon_location_id'])) {
                        $locations = $this->dealFactory->create()->getCollection()
                            ->addFieldToFilter('product_id', $productId)
                            ->getFirstItem()
                            ->getData('location');

                        $locations = unserialize($locations);

                        foreach ($locations as $location) {
                            if ($location['record_id'] == $data['additional_options']['groupon_location_id']) {
                                $additionalOptions[] = [
                                    'label' => 'Location',
                                    'value' => $location['street'] . "-" . $location['city'] . "-" . $location['country'],
                                ];
                            }
                        }
                    }
                    $item->setAdditionalData(serialize($additionalOptions));
                }
            }

            if ($checkTypeProduct == 'coupon') {
                if (isset($data['additional_options'])) {
                    $additionalOptions = [];

                    if (isset($data['additional_options']['groupon_location_id'])) {
                        $locations = $this->dealFactory->create()->getCollection()
                            ->addFieldToFilter('product_id', $productId)
                            ->getFirstItem()
                            ->getData('location');

                        $locations = unserialize($locations);

                        foreach ($locations as $location) {
                            if ($location['record_id'] == $data['additional_options']['groupon_location_id']) {
                                $additionalOptions[] = [
                                    'label' => 'Location',
                                    'value' => $location['street'] . "-" . $location['city'] . "-" . $location['country'],
                                ];
                            }
                        }
                    }
                    $array = [
                        'code' => 'additional_options',
                        'value' => serialize($additionalOptions)
                    ];
                    $item->addOption($array);
                }
            }
        } catch (\Exception $e) {
            \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->debug('Add to cart exception: '.$e->getMessage());
        }
    }
}
