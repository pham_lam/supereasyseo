# CHANGELOG #

### 1.0.0 ###

* Add new Configurable Product has children "coupon product"
* Add new product type: Coupon
* Add new tab: Groupon, Location Option in product edit/new page panel
* Add coordinates, background in PDF Template, time data
* Add attachted PDF file in email 
* Add option
* Deployment instructions
* Show product(s) custom on product detail page