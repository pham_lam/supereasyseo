<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class Deal
 *
 * @package Magenest\Groupon\Model
 *
 * @method int getDealId()
 * @method int getProductId()
 * @method int getStoreId()
 * @method int getParentId()
 * @method string getProductType()
 * @method string getStartTime()
 * @method string getEndTime()
 * @method string getReminderBeforeDay()
 * @method string getReminderTemplate()
 * @method int getVendorId()
 * @method int getMinimumBuyersLimit()
 * @method int getMaximumBuyersLimit()
 * @method int getEnable()
 * @method string getAllowCreditPay()
 * @method int getViewCount()
 * @method int getPurchasedCount()
 * @method int getStatus()
 * @method string getLocation()
 * @method string getGrouponExpire()
 * @method int getPriority()
 * @method int getUserLimit()
 */
class Deal extends AbstractModel
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\ResourceModel\Deal');
    }
}
