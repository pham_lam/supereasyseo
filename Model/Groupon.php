<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\App\Area;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\Groupon\Helper\Pdf;
use Magenest\Groupon\Model\Mail\Template\TransportBuilder;

/**
 * Class Groupon
 *
 * @package Magenest\Groupon\Model
 *
 * @method int getGrouponId()
 * @method int getProductId()
 * @method string getGrouponCode()
 * @method string getRedemptionCode()
 * @method int getOrderId()
 * @method int getStatus()
 * @method string getCreatedAt()
 * @method string getUpdatedAt()
 * @method int getQty()
 * @method string getProductName()
 * @method string getLocation()
 * @method int getCustomerId()
 * @method int getCustomerEmail()
 * @method int getCustomerName()
 */
class Groupon extends AbstractModel
{
    /**
     * Const Email
     */
    const XML_PATH_EMAIL_SENDER = 'trans_email/ident_general/email';

    /**
     * Const Name
     */
    const XML_PATH_NAME_SENDER = 'trans_email/ident_general/name';

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Pdf
     */
    protected $_pdf;

    /**
     * @var Ticket
     */
    protected $ticket;

    /**
     * @var Option
     */
    protected $option;


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Groupon\Model\ResourceModel\Groupon');
    }

    /**
     * Groupon constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $scopeConfig
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param StoreManagerInterface $storeManager
     * @param Pdf $pdf
     * @param Ticket $ticket
     * @param Option $option
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $scopeConfig,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        StoreManagerInterface $storeManager,
        Pdf $pdf,
        array $data = []
    ) {
        parent::__construct($context, $registry);
        $this->_scopeConfig = $scopeConfig;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->_storeManager = $storeManager;
        $this->_pdf = $pdf;
    }

    /**
     * Send Mail to customer
     *
     * @param $eventName
     */
    public function sendMail($grouponId)
    {
//        if (!$this->getId()) {
//            return;
//        }
        $data = $this->load($grouponId);
        $pdf = $this->_pdf->getPdf($data);
        $file = $pdf->render();
        $this->inlineTranslation->suspend();
        $transport = $this->_transportBuilder->setTemplateIdentifier('groupon_mail_template')->setTemplateOptions(
            [
                'area' => Area::AREA_FRONTEND,
                'store' => $this->_storeManager->getStore()->getId(),
            ]
        )->setTemplateVars(
            [
                'store' => $this->_storeManager->getStore(),
                'store URL' => $this->_storeManager->getStore()->getBaseUrl(),
                'groupon_code' => $data->getGrouponCode(),
                'create' => $data->getCreatedAt(),
                'qty' => $data->getQty(),
            ]
        )->setFrom(
            [
                'email' => $this->_scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER),
                'name' => $this->_scopeConfig->getValue(self::XML_PATH_NAME_SENDER)
            ]
        )->addTo(
            $this->getCustomerEmail(),
            $this->getCustomerName()
        )->createAttachment($file)->getTransport();
        $transport->sendMessage();
        $this->inlineTranslation->resume();
    }
}
