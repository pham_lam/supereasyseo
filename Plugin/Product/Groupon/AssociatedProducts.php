<?php
namespace Magenest\Groupon\Plugin\Product\Groupon;

use Magento\Catalog\Model\ProductFactory;

class AssociatedProducts
{
    const SHORT_TITLE = 'short_title';
    const COMMISSION = 'commission';
    const AFFILIATE = 'affiliate';

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * AssociatedProducts constructor.
     * @param ProductFactory $productFactory
     */
    public function __construct(ProductFactory $productFactory)
    {
        $this->productFactory = $productFactory;
    }

    /**
     * @param \Magento\ConfigurableProduct\Ui\DataProvider\Product\Form\Modifier\Data\AssociatedProducts $subject
     * @param $result
     * @return string
     */
    public function afterGetProductMatrix(\Magento\ConfigurableProduct\Ui\DataProvider\Product\Form\Modifier\Data\AssociatedProducts $subject, $result)
    {
        $temp = $result;
        try {
            foreach ($result as $key => $product) {
                $productModel = $this->productFactory->create()->load($product['id']);
                if ($productModel->getTypeId() == 'coupon') {
                    $result[$key][self::SHORT_TITLE] = $productModel->getData(self::SHORT_TITLE);
                    $result[$key][self::COMMISSION] = $productModel->getData(self::COMMISSION);
                    $result[$key][self::AFFILIATE] = $productModel->getData(self::AFFILIATE);
                }
            }
        } catch (\Exception $e) {
            \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->debug('Product Matrix Plugin Exception: '.$e->getMessage());
            $result = $temp;
        } finally {
            return $result;
        }
    }
}
