<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\ProductFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Directory\Model\CountryFactory;
use Magenest\Groupon\Model\GrouponFactory;
use Magenest\Groupon\Model\TicketFactory;
use Magenest\Groupon\Model\DealFactory;

/**
 * Class Pdf
 *
 * @package Magenest\Ticket\Helper
 */
class Pdf extends AbstractHelper
{
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var File
     */
    protected $fileFramework;

    /**
     * @var Template
     */
    protected $template;

    /**
     * @var OptionFactory
     */
    protected $option;

    /**
     * @var TicketFactory
     */
    protected $ticket;

    /**
     * @var DealFactory
     */
    protected $deal;

    /**
     * @var ProductFactory
     */
    protected $product;

    /**
     * @var OrderFactory
     */
    protected $order;

    /**
     * @var Country
     */
    protected $country;


    /**
     * Pdf constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param Filesystem $filesystem
     * @param File $fileFramework
     * @param Template $template
     * @param OptionFactory $optionFactory
     * @param TicketFactory $ticketFactory
     * @param DealFactory $dealFactory
     * @param ProductFactory $productFactory
     * @param OrderFactory $orderFactory
     * @param CountryFactory $country
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Filesystem $filesystem,
        File $fileFramework,
        Template $template,
        GrouponFactory $grouponFactory,
        TicketFactory $ticketFactory,
        DealFactory $dealFactory,
        ProductFactory $productFactory,
        OrderFactory $orderFactory,
        CountryFactory $country
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->filesystem = $filesystem;
        $this->fileFramework = $fileFramework;
        $this->template = $template;
        $this->groupon = $grouponFactory;
        $this->ticket = $ticketFactory;
        $this->deal = $dealFactory;
        $this->product = $productFactory;
        $this->order = $orderFactory;
        $this->country = $country;
    }

    /**
     * @param $model
     * @return \Zend_Pdf
     */
    public function getPdf($model)
    {
        /** @var \Magenest\Groupon\Model\Groupon $model */
        $pdf = new \Zend_Pdf();
        $modelTicket = $this->ticket->create()->load($model->getProductId(), 'product_id');

        $font_regular = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $width = $modelTicket->getPageWidth();
        $height = $modelTicket->getPageHeight();

        $size = $width. ':'. $height;
        $page = $pdf->newPage($size);
        $backgroundLink = unserialize($modelTicket->getBackground());
        if (isset($backgroundLink) && !empty($backgroundLink)) {
            $background = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath('groupon/template/'.$backgroundLink['0']['file']);
            if (is_file($background)) {
                $image = \Zend_Pdf_Image::imageWithPath($background);
                $page->drawImage($image, 0, 0, $width, $height);
            }
        }

        $code = $model->getRedemptionCode();
        $page->setFont($font_regular, 15);
        $coordinates = $modelTicket->getCoordinates();
        $tableRowsArr = [];
        if (@unserialize($coordinates)) {
            $tableRowsArr = unserialize($coordinates);
        }

        foreach ($tableRowsArr as $param) {
            /**
             * Insert QR Code to PDF File
             */
            if (!empty($param['info']) && $param['info'] == 'qr_code' && !empty($param['x']) && !empty($param['y']) && !empty($param['size'])) {
                $fileName = $this->getQrCode($code);
                $pathQrcode = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath($fileName);
                $image = \Zend_Pdf_Image::imageWithPath($pathQrcode);
                $page->drawImage(
                    $image,
                    $param['x'],
                    $param['y'],
                    $param['x'] + $param['size'],
                    $param['y'] + $param['size']
                );

                unlink($pathQrcode);
                continue;
            }

            /**
             * Insert Barcode to PDF File
             */
            if (!empty($param['info']) && $param['info'] == 'bar_code') {
                $barcodeOptions = ['text' => $code, 'drawText' => false];
                $rendererOptions = [];
                $imageResource = \Zend_Barcode::draw('code128', 'image', $barcodeOptions, $rendererOptions);
                $barcode = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath('barcode.jpg');
                imagejpeg($imageResource, $barcode, 100);
                imagedestroy($imageResource);
                $image = \Zend_Pdf_Image::imageWithPath($barcode);
                $page->drawImage(
                    $image,
                    $param['x'],
                    $param['y'],
                    $param['x'] + $param['size']*4,
                    $param['y'] + $param['size']
                );
                unlink($barcode);
                continue;
            }

            /**
             * Insert diffence information
             */
            if (!empty($param['info']) && !empty($param['x']) && !empty($param['y']) && !empty($param['size']) && !empty($param['color'])) {
                $page->setFont($font_regular, $param['size']);
                $color = new \Zend_Pdf_Color_Html($param['color']);
                $page->setFillColor($color);
                $text = $this->replaceByTicket($model, $param['info']);
                if (!empty($param['title'])) {
                    $resultText = $param['title'].': '.$text;
                } else {
                    $resultText = $text;
                }
                $page->drawText(
                    $resultText,
                    $param['x'],
                    $param['y'],
                    'UTF-8'
                );
            }
        }
        $pdf->pages[] = $page;

        return $pdf;
    }

    /**
     * @param $model
     * @param $info
     * @return string
     */
    public function replaceByTicket($model, $info)
    {
        /** @var \Magenest\Groupon\Model\Groupon $model */

        /** @var \Magenest\Groupon\Model\Ticket $ticket */
        $ticket = $this->ticket->create()->load($model->getProductId(), 'product_id');

        /** @var \Magenest\Groupon\Model\Deal $deal */
        $deal = $this->deal->create()->load($model->getProductId(), 'product_id');

        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->order->create()->load($model->getOrderId());
        $currencySym = $order->getOrderCurrency()->getCurrencySymbol();


        $text = '';

        switch ($info) {
            case 'event_name':
                $text = $model->getProductName();
                break;
            case 'price':
                $text =$currencySym.$order->getGrandTotal();
                break;
            case 'expire':
                $text = $deal->getGrouponExpire();
                break;
            case 'location':
                $text = $model->getLocation();
                break;
            case 'groupon_code':
                $text = $model->getGrouponCode();
                break;
            case 'redemption_code':
                $text = $model->getRedemptionCode();
                break;
            case 'customer_name':
                $text = $model->getCustomerName();
                break;
            case 'customer_email':
                $text = $model->getCustomerEmail();
                break;
            case 'order_increment_id':
                $text = $order->getIncrementId();
                break;
            case 'order_date':
                $text = $model->getCreatedAt();
                break;
            case 'qty':
                $text = $model->getQty();
                break;
            case 'term':
                $text = $ticket->getTerms();
                break;
            case 'instructions':
                $text = $ticket->getInstructions();
                break;
            default:
                break;
        }

        return $text;
    }
//    /**
//     * @return \Zend_Pdf
//     * @throws \Zend_Pdf_Exception
//     */
//    public function getPdf($groupon)
//    {
//        $pdf = new \Zend_Pdf();
//        $fontRegular = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
//        $fontRegularBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);
//        $width = 945;
//        $height = 740;
//
//        $size = $width. ':'. $height;
//        $page = $pdf->newPage($size);
//        $background = $this->filesystem->getDirectoryWrite(DirectoryList::APP)->getAbsolutePath('code/Magenest/Groupon/view/adminhtml/web/images/gosawa1.jpeg');
//        if (is_file($background)) {
//            $image = \Zend_Pdf_Image::imageWithPath($background);
//            $page->drawImage($image, 0, 0, $width, $height);
//        }
//        $productId = $groupon->getProductId();
//        $modelDeal = $this->deal->create()->load($productId, 'product_id');
//        $modelTicket = $this->ticket->create()->load($productId, 'product_id');
//        $modelOption = $this->ticket->create()->load($groupon->getOptionId());
//        $modelProduct = $this->product->create()->load($productId);
//        $modelOrder = $this->order->create()->loadByIncrementId($groupon->getOrderId());
//
//        $code = $groupon->getRedemptionCode();
//        $page->setFont($fontRegular, 15);
//
//        $path = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
//
//        /**
//         * Insert QR Code to PDF File
//         */
//        $fileName = $this->getQrCode($code);
//        $pathQrcode = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath($fileName);
//        $image = \Zend_Pdf_Image::imageWithPath($pathQrcode);
//        $page->drawImage($image, 50, 370, 170, 490);
//
//        if ($path->isFile($fileName)) {
//            $this->filesystem->getDirectoryWrite(
//                DirectoryList::MEDIA
//            )->delete($fileName);
//        }
//        $page->setFont($fontRegularBold, 18);
//        $page->drawText($modelProduct->getName(), 310, 620, 'UTF-8');
//        $page->drawText('Value: $100.25', 740, 370, 'UTF-8');
//
//        $location = unserialize($modelDeal->getLocation());
//        $locationArray = $location[$groupon->getLocation()];
//        $street = $locationArray['street'];
//        $city = $locationArray['city'];
//        $countryName = $this->country->create()->loadByCode($locationArray['country'])->getName();
//
//        /**
//         * Insert diffenceinformation
//         */
//        $page->setFont($fontRegular, 13);
//        $color = new \Zend_Pdf_Color_Html('black');
//        $page->setFillColor($color);
//        $array = [
//            'Redeem At: ' => [40,220],
//            $street => [40, 200],
//            $city => [40, 185],
//            $countryName => [40, 170],
//            'Instructions: ' => [430, 220],
//            $modelTicket->getInstructions() => [430, 195],
//            'Terms' => [40, 320],
//            $modelTicket->getTerms() => [40, 300],
//            'Name: '.$modelOrder->getCustomerName()=> [210, 415],
//            'Redeemable from: '.$modelDeal->getStartTime() => [210, 400],
//            'Expires On: '.$modelDeal->getGrouponExpire() => [210, 385],
//            'Redeemption Code: '.$groupon->getRedemptionCode() => [210, 370],
//            $groupon->getGrouponCode() => [370, 600]
//
//        ];
//
//        $i = 0;
//        foreach ($array as $key=>$value) {
//            $page->drawText(
//                $key,
//                $value[0],
//                $value[1],
//                'UTF-8'
//            );
//            $i ++;
//        }
//
//        $pdf->pages[] = $page;
//
//        return $pdf;
//    }

    public function getQrCode($code)
    {
        $url = "http://api.qrserver.com/v1/create-qr-code/?&size=120x120&data=" . $code;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $raw = curl_exec($ch);
        curl_close($ch);
        $path = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)
            ->getAbsolutePath("qr_".$code.".png");
        if (file_exists($path)) {
            unlink($path);
        }
        $fp = $this->fileFramework->fileOpen($path, 'x');
        $this->fileFramework->fileWrite($fp, $raw);
        $this->fileFramework->fileClose($fp);
        $file = 'qr_'.$code.".png";

        return  $file;
    }

    /**
     * Print PDF Template Preview
     *
     * @param array $data
     * @return \Zend_Pdf
     * @throws \Zend_Pdf_Exception
     */
    public function getPrintPdfPreview($data)
    {
        $pdf = new \Zend_Pdf();
        $fontRegular = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA);
        $fontRegularBold = \Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD);

        $width = 945;
        $height = 740;

        $size = $width. ':'. $height;
        $page = $pdf->newPage($size);
        $background = $this->filesystem->getDirectoryWrite(DirectoryList::APP)->getAbsolutePath('code/Magenest/Groupon/view/adminhtml/web/images/gosawa1.jpeg');
        if (is_file($background)) {
            $image = \Zend_Pdf_Image::imageWithPath($background);
            $page->drawImage($image, 0, 0, $width, $height);
        }

        $code = 'MagenestA4vM';
        $page->setFont($fontRegular, 15);

        $path = $this->filesystem->getDirectoryRead(
            DirectoryList::MEDIA
        );

    /**
     * Insert QR Code to PDF File
     */
        $fileName = $this->getQrCode($code);
        $pathQrcode = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath($fileName);
        $image = \Zend_Pdf_Image::imageWithPath($pathQrcode);
        $page->drawImage($image, 50, 370, 170, 490);

        if ($path->isFile($fileName)) {
            $this->filesystem->getDirectoryWrite(
                DirectoryList::MEDIA
            )->delete($fileName);
        }
        $page->setFont($fontRegularBold, 18);
        $page->drawText('Best Restaurant & Lounger Voucher', 310, 620, 'UTF-8');
        $page->drawText('Value: $100', 740, 370, 'UTF-8');

        /**
         * Insert diffenceinformation
         */
        $page->setFont($fontRegular, 13);
        $color = new \Zend_Pdf_Color_Html('black');
        $page->setFillColor($color);
        $array = [
            'Redeem At: ' => [40,220],
            'Abdel Aziz Street' => [40, 200],
            'Beirut' => [40, 185],
            'Lebanon' => [40, 170],
            'Instructions: ' => [430, 220],
            'instructions information' => [430, 195],
            'Terms' => [40, 320],
            'terms information' => [40, 300],
            'Name: John Doe' => [210, 415],
            'Redeemable from: Anytime' => [210, 400],
            'Expires On: 01/20/2017 3:14 AM' => [210, 385],
            'Redeemption Code: 123assfs24333' => [210, 370],
            '#1234-2113-34ACDD532' => [370, 600]

        ];
        $i = 0;
        foreach ($array as $key => $value) {
            $page->drawText(
                $key,
                $value[0],
                $value[1],
                'UTF-8'
            );
            $i ++;
        }

        $pdf->pages[] = $page;

        return $pdf;
    }
}
