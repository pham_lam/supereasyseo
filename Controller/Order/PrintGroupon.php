<?php
/**
 * Copyright © 2015 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Controller\Order;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magenest\Groupon\Helper\Pdf;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Action;
use Magenest\Groupon\Model\GrouponFactory;

/**
 * Class PrintGroupon
 * @package Magenest\Groupon\Controller\Order
 */
class PrintGroupon extends Action
{
    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @var Pdf
     */
    protected $pdfGroupon;

    /**
     * @var GrouponFactory
     */
    protected $groupon;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * PrintTicket constructor.
     * @param Context $context
     * @param DateTime $dateTime
     * @param FileFactory $fileFactory
     * @param Pdf $pdfGroupon
     * @param CustomerSession $customerSession
     * @param GrouponFactory $grouponFactory
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        DateTime $dateTime,
        FileFactory $fileFactory,
        Pdf $pdfGroupon,
        CustomerSession $customerSession,
        GrouponFactory $grouponFactory,
        PageFactory $resultPageFactory
    ) {
        $this->fileFactory = $fileFactory;
        $this->dateTime = $dateTime;
        $this->pdfGroupon = $pdfGroupon;
        $this->_customerSession = $customerSession;
        $this->groupon = $grouponFactory;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\View\Result\Page
     * @throws \Exception
     * @throws \Zend_Pdf_Exception
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        if (!($customerId = $this->_customerSession->getCustomerId())) {
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('/');
        }

        $grouponId = (int)$this->getRequest()->getParam('groupon_id');
        if ($grouponId) {
            $modelGroupon = $this->groupon->create()->load($grouponId);
            if ($modelGroupon->getId() && $customerId == $modelGroupon->getCustomerId()) {
                return $this->fileFactory->create(
                    sprintf('groupon%s.pdf', $this->dateTime->date('Y-m-d_H-i-s')),
                    $this->pdfGroupon->getPdf($modelGroupon)->render(),
                    DirectoryList::VAR_DIR,
                    'application/pdf'
                );
            }
        }
        return $resultPage;
    }
}
