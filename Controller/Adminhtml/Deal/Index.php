<?php
namespace Magenest\Groupon\Controller\Adminhtml\Deal;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 * @package Magenest\Groupon\Controller\Adminhtml\Deal
 */
class Index extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {

        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Groupon::deal');
        $resultPage->addBreadcrumb(__('Deal'), __('Deal'));
        $resultPage->addBreadcrumb(__('Manage Deal'), __('Manage Deal'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Deal'));
        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Groupon::deal');
    }
}
