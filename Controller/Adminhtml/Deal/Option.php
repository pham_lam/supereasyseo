<?php
namespace Magenest\Groupon\Controller\Adminhtml\Deal;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Option
 *
 * @package Magenest\Groupon\Controller\Adminhtml\Deal
 */
class Option extends \Magento\Backend\App\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Index constructor
     *
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Groupon::deal_option');
        $resultPage->addBreadcrumb(__('Deal Option'), __('Deal Option'));
        $resultPage->addBreadcrumb(__('Manage Deal Option'), __('Manage Deal Option'));
        $resultPage->getConfig()->getTitle()->prepend(__('Manage Deal Option'));

        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Groupon::deal_option');
    } //end
}
