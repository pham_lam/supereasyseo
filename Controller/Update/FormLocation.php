<?php
/**
 * Created by PhpStorm.
 * User: canhnd
 * Date: 20/01/2017
 * Time: 16:01
 */
namespace Magenest\Groupon\Controller\Update;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory as ResultJsonFactory;
use Magenest\Groupon\Model\DealFactory;

/**
 * Class FormLocation
 * @package Magenest\Groupon\Controller\Update
 */
class FormLocation extends \Magento\Framework\App\Action\Action
{

    /**
     * @var ResultJsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var DealFactory
     */
    protected $deal;

    /**
     * Location constructor.
     * @param Context $context
     * @param ResultJsonFactory $resultJsonFactory
     * @param DealFactory $dealFactory
     */
    public function __construct(
        Context $context,
        ResultJsonFactory $resultJsonFactory,
        DealFactory $dealFactory
    ) {
        $this->deal = $dealFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * execute
     */
    public function execute()
    {
        $data = $this->_getDataJson();
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData($data);
    }

    /**
     * @return array
     */
    protected function _getDataJson()
    {

        $data = $this->getRequest()->getParams();
        if (!(isset($data['product_id']) && $data['product_id'])) {
            return [];
        }
        $dealModel = $this->deal->create()->load($data['product_id'], 'product_id');

//        $logger = \Magento\Framework\App\ObjectManager::getInstance()->create('Psr\Log\LoggerInterface')->debug(print_r($productId, true));

        $location = unserialize($dealModel->getLocation());

        return $location;
    }
}
