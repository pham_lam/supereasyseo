<?php
/**
 * Created by PhpStorm.
 * User: canhnd
 * Date: 20/01/2017
 * Time: 16:01
 */
namespace Magenest\Groupon\Controller\Update;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory as ResultJsonFactory;
use Magenest\Groupon\Model\TicketFactory;

/**
 * Class Hightlight
 * @package Magenest\Groupon\Controller\Update
 */
class Highlight extends \Magento\Framework\App\Action\Action
{

    /**
     * @var ResultJsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var TicketFactory
     */
    protected $ticket;

    /**
     * Highlight constructor.
     * @param Context $context
     * @param ResultJsonFactory $resultJsonFactory
     * @param TicketFactory $ticketFactory
     */
    public function __construct(
        Context $context,
        ResultJsonFactory $resultJsonFactory,
        TicketFactory $ticketFactory
    ) {
        $this->ticket = $ticketFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * execute
     */
    public function execute()
    {
        $data = $this->_getDataJson();
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData($data);
    }

    /**
     * @return string
     */
    protected function _getDataJson()
    {

        $data = $this->getRequest()->getParams();
        if (!(isset($data['product_id']) && $data['product_id'])) {
            return [];
        }
        $productId = $data['product_id'];
        $ticketModel = $this->ticket->create()->load($productId, 'product_id');
        $terms = $ticketModel->getTerms() ? $ticketModel->getTerms() : 'No data';
        $instructions = $ticketModel->getInstructions() ? $ticketModel->getInstructions() : 'No data';
        $array = [
            'terms' => $terms,
            'instruct' => $instructions
        ];

        return $array;
    }
}
