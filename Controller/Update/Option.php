<?php
/**
 * Created by PhpStorm.
 * User: canhnd
 * Date: 20/01/2017
 * Time: 16:01
 */
namespace Magenest\Groupon\Controller\Update;

use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory as ResultJsonFactory;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Option
 *
 * @package Magenest\Groupon\Controller\Update
 */
class Option extends \Magento\Framework\App\Action\Action
{

    /**
     * @var ResultJsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var productFactory
     */
    protected $productFactory;

    /**
     * @var StockStateInterface
     */
    protected $stockStateInterface;

    protected $logger;

    /**
     * Option constructor.
     * @param Context $context
     * @param ResultJsonFactory $resultJsonFactory
     * @param ProductFactory $productFactory
     * @param StockStateInterface $stockStateInterface
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        ResultJsonFactory $resultJsonFactory,
        ProductFactory $productFactory,
        StockStateInterface $stockStateInterface,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
        $this->productFactory = $productFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->stockStateInterface = $stockStateInterface;
        parent::__construct($context);
    }

    /**
     * execute
     */
    public function execute()
    {
        $data = $this->_getDataJson();
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultPage->setData($data);
        return $resultPage;
    }

    /**
     * @return array
     */
    protected function _getDataJson()
    {
        $data = $this->getRequest()->getParams();
        if (!(isset($data['option_id']) && $data['option_id'])) {
            return [];
        }
        $product = $this->productFactory->create()->load($data['option_id']);
        $dealPrice = $product->getFinalPrice();
        if (isset($data['parent_product_id'])) {
            $parentProduct = $this->productFactory->create()->load($data['parent_product_id']);
            $dealPrice = $parentProduct->getData('price');
        }
        $qtyInStock = $this->stockStateInterface->getStockQty($product->getId(), $product->getStore()->getWebsiteId());
        $save = round($dealPrice - $product->getPrice(), 2);
        $price = round($product->getPrice(), 2);
        $discount = ($price == 0) ? '0' :  ((round($save/($save+$price), 4))*100).'%';
        $data = [
            'value' => $price,
            'you_save' => $save,
            'discount' => $discount,
            'qty'   => $qtyInStock,
        ];
        return $data;
    }
}
