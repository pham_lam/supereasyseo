<?php

namespace Magenest\Groupon\Ui\Component;

/**
 * Class Company
 * @package Magenest\Groupon\Ui\Component
 */
class Company implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Company 1'),
                'value' => '1'
            ],
            [
                'label' => __('Company 2'),
                'value' => '2'
            ],
            [
                'label' => __('Company 3'),
                'value' => '3'
            ],
        ];
    }
}
