<?php
namespace Magenest\Groupon\Ui\Component;

/**
 * Class Country
 * @package Magenest\Groupon\Ui\Component
 */
class Country implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $country;

    /**
     * Country constructor.
     * @param \Magento\Directory\Model\Config\Source\Country $country
     */
    public function __construct(
        \Magento\Directory\Model\Config\Source\Country $country
    ) {
        $this->country = $country;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return $this->country->toOptionArray();
    }
}
