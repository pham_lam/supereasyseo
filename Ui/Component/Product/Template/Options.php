<?php

namespace Magenest\Groupon\Ui\Component\Product\Template;

class Options implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Name'),
                'value' => 'event_name'
            ],
            [
                'label' => __('Price'),
                'value' => 'price'
            ],
            [
                'label' => __('Expire'),
                'value' => 'expire'
            ],
            [
                'label' => __('Location'),
                'value' => 'location'
            ],
            [
                'label' => __('QR Code'),
                'value' => 'qr_code'
            ],
            [
                'label' => __('Bar Code'),
                'value' => 'bar_code'
            ],
            [
                'label' => __('Groupon Code'),
                'value' => 'groupon_code'
            ],
            [
                'label' => __('Redemption Code'),
                'value' => 'redemption_code'
            ],
            [
                'label' => __('Customer Name'),
                'value' => 'customer_name'
            ],
            [
                'label' => __('Customer Email'),
                'value' => 'customer_email'
            ],
            [
                'label' => __('Order #'),
                'value' => 'order_increment_id'
            ],
            [
                'label' => __('Order Date'),
                'value' => 'order_date'
            ],
            [
                'label' => __('Quantity'),
                'value' => 'qty'
            ],
            [
                'label' => __('Term'),
                'value' => 'term'
            ],
            [
                'label' => __('Instructions'),
                'value' => 'instructions'
            ]

        ];
    }
}
