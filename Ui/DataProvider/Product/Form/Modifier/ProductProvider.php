<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\Groupon\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ProductOptions\ConfigInterface;
use Magento\Catalog\Model\Config\Source\Product\Options\Price as ProductOptionsPrice;
use Magento\Framework\UrlInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\DynamicRows;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Element\ActionDelete;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magento\Ui\Component\Form\Element\Checkbox;
use Magento\Framework\Locale\CurrencyInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\App\RequestInterface;
use Magenest\Groupon\Model\DealFactory;
use Magenest\Groupon\Model\TicketFactory;
use Psr\Log\LoggerInterface;
use Magenest\Groupon\Ui\Component\Country;

/**
 * Data provider for "Customizable Options" panel
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ProductProvider extends AbstractModifier
{
    const PRODUCT_TYPE = 'configurable';
    const PRODUCT_TYPE_COUPON = 'coupon';
    const CONTROLLER_ACTION_EDIT_PRODUCT = 'catalog_product_edit';
    const CONTROLLER_ACTION_NEW_PRODUCT = 'catalog_product_new';
    const EVENT_TICKET_TAB = 'event';

    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var DealFactory
     */
    protected $deal;

    /**
     * @var TicketFactory
     */
    protected $ticket;
    /**#@+
     * Group values
     */
    const GROUP_CUSTOM_OPTIONS_NAME = 'location_options';
    const GROUP_CUSTOM_OPTIONS_SCOPE = 'data.groupon';
    /**#@-*/
    /**#@-*/



    /**#@+
     * Grid values
     */
    const GRID_TYPE_LOCATION = 'location';
    /**#@-*/

    /**#@+
     * Field values
     */

    const FIELD_STREET_NAME = 'street';
    const FIELD_CITY_NAME = 'city';
    const FIELD_COUNTRY_NAME = 'country';
    const FIELD_IS_REQUIRE = 'is_require';
    const FIELD_SORT_ORDER_NAME = 'sort_order';
    const FIELD_IS_DELETE = 'is_delete';

    /**
     * @var \Magento\Catalog\Model\Locator\LocatorInterface
     */
    protected $locator;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductOptions\ConfigInterface
     */
    protected $productOptionsConfig;

    /**
     * @var \Magento\Catalog\Model\Config\Source\Product\Options\Price
     */
    protected $productOptionsPrice;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var ArrayManager
     */
    protected $arrayManager;

    /**
     * @var array
     */
    protected $meta = [];

    /**
     * @var CurrencyInterface
     */
    protected $localeCurrency;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Country
     */
    protected $country;

    /**
     * ProductProvider constructor.
     * @param LocatorInterface $locator
     * @param StoreManagerInterface $storeManager
     * @param ConfigInterface $productOptionsConfig
     * @param ProductOptionsPrice $productOptionsPrice
     * @param UrlInterface $urlBuilder
     * @param ArrayManager $arrayManager
     * @param DealFactory $dealFactory
     * @param TicketFactory $ticketFactory
     * @param RequestInterface $request
     */
    public function __construct(
        LocatorInterface $locator,
        StoreManagerInterface $storeManager,
        ConfigInterface $productOptionsConfig,
        ProductOptionsPrice $productOptionsPrice,
        UrlInterface $urlBuilder,
        ArrayManager $arrayManager,
        DealFactory $dealFactory,
        TicketFactory $ticketFactory,
        RequestInterface $request,
        LoggerInterface $logger,
        Country $country
    ) {
        $this->logger = $logger;
        $this->locator = $locator;
        $this->storeManager = $storeManager;
        $this->productOptionsConfig = $productOptionsConfig;
        $this->productOptionsPrice = $productOptionsPrice;
        $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
        $this->deal = $dealFactory;
        $this->ticket = $ticketFactory;
        $this->request = $request;
        $this->country = $country;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        $product = $this->locator->getProduct();
        $productId = $product->getId();
//            $this->logger->debug('aaaaaa');

        /** @var \Magenest\Groupon\Model\Deal $modelDeal */
        $modelDeal = $this->deal->create()->load($productId, 'product_id');
        if ($modelDeal && !empty($modelDeal->getData())) {
            //data deal
            $data[strval($productId)]['groupon'] = [
                'enable_groupon' => $modelDeal->getEnable(),
                'priority' => $modelDeal->getPriority(),
                'minimum' => $modelDeal->getMinimumBuyersLimit(),
                'maximum' => $modelDeal->getMaximumBuyersLimit(),
                'groupon_per_user_limit' => $modelDeal->getUserLimit(),
                'start_time' => $modelDeal->getStartTime(),
                'end_time' => $modelDeal->getEndTime(),
                'redeemable_after' => $modelDeal->getReminderBeforeDay(),
                'groupon_expire' => $modelDeal->getGrouponExpire(),
                'company' => $modelDeal->getVendorId(),
            ];

            /** @var \Magenest\Groupon\Model\Ticket $modelTicket */
            $modelTicket = $this->ticket->create()->load($productId, 'product_id');
            if (!empty($modelTicket->getData())) {
                //data on pdftemplate
                $data[strval($productId)]['groupon']['ticket'] = [
                    'page_width' => $modelTicket->getPageWidth(),
                    'page_height' => $modelTicket->getPageHeight(),
                    'terms' => $modelTicket->getTerms(),
                    'instructions' => $modelTicket->getInstructions(),
                ];
                $coordinates = $modelTicket->getCoordinates();
                if (!empty($coordinates)) {
                    $arrayCoor = unserialize($coordinates);
                    $size = sizeof($arrayCoor);
                    for ($i = 0; $i < $size; $i++) {
                        $data[strval($productId)]['groupon']['ticket']['coordinates'][$i] =
                            [
                                'record_id' => $i,
                                'info' => $arrayCoor[$i]['info'],
                                'title' => $arrayCoor[$i]['title'],
                                'x' => $arrayCoor[$i]['x'],
                                'y' => $arrayCoor[$i]['y'],
                                'size' => $arrayCoor[$i]['size'],
                                'color' => $arrayCoor[$i]['color'],
                            ];
                    }
                }
                $background = $modelTicket->getBackground();
                if (@unserialize($background) !== false) {
                    $data[$productId]['groupon']['ticket']['background'] = unserialize($background);
                }
            }

            // data location
            if ($this->isCoupon() || $product->getFeaturedDeal()) {
                if (!empty($modelDeal->getLocation())) {
                    $arrayLocation = unserialize($modelDeal->getLocation());
                    $size = sizeof($arrayLocation);
                    for ($i = 0; $i < $size; $i++) {
                        $data[strval($productId)]['groupon']['location'][$i] =
                            [
                                'record_id' => $i,
                                'street' => $arrayLocation[$i]['street'],
                                'city' => $arrayLocation[$i]['city'],
                                'country' => $arrayLocation[$i]['country'],
                                'is_require' => $arrayLocation[$i]['is_require'],
                            ];
                    }
                }
            }
        }

        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;
        if ($this->isCoupon() || isset($meta['product-details']['children']['container_commission'])
        ) {
            $this->createLocationPanel();
        }

        return $this->meta;
    }

    /**
     * Create "Customizable Options" panel
     *
     * @return $this
     */
    protected function createLocationPanel()
    {
        $this->meta = array_replace_recursive(
            $this->meta,
            [
                static::GROUP_CUSTOM_OPTIONS_NAME => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Location Options'),
                                'componentType' => Fieldset::NAME,
                                'dataScope' => static::GROUP_CUSTOM_OPTIONS_SCOPE,
                                'collapsible' => true,
                                'sortOrder' => 10
                            ],
                        ],
                    ],
                    'children' => [
                        static::GRID_TYPE_LOCATION => $this->getSelectTypeGridConfig(10),
                    ]
                ]
            ]
        );

        return $this;
    }


    /**
     * Get config for grid for "select" types
     *
     * @param int $sortOrder
     * @return array
     */
    protected function getSelectTypeGridConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'addButtonLabel' => __('Add Value'),
                        'componentType' => DynamicRows::NAME,
                        'component' => 'Magento_Ui/js/dynamic-rows/dynamic-rows',
                        'additionalClasses' => 'admin__field-wide',
                        'deleteProperty' => static::FIELD_IS_DELETE,
                        'deleteValue' => '1',
                        'renderDefaultRecord' => false,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'positionProvider' => static::FIELD_SORT_ORDER_NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                            ],
                        ],
                    ],
                    'children' => [
                        static::FIELD_STREET_NAME => $this->getStreetFieldConfig(10),
                        static::FIELD_CITY_NAME => $this->getCityFieldConfig(20),
                        static::FIELD_COUNTRY_NAME => $this->getCountryFieldConfig(30, ['fit' => true]),
                        static::FIELD_IS_REQUIRE => $this->getRequireFieldConfig(40),
                        static::FIELD_IS_DELETE => $this->getIsDeleteFieldConfig(50)
                    ]
                ]
            ]
        ];
    }

    /**
     * Get config for "Title" fields
     *
     * @param int $sortOrder
     * @param array $options
     * @return array
     */
    protected function getStreetFieldConfig($sortOrder, array $options = [])
    {
        return array_replace_recursive(
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Street'),
                            'componentType' => Field::NAME,
                            'formElement' => Input::NAME,
                            'dataScope' => static::FIELD_STREET_NAME,
                            'dataType' => Text::NAME,
                            'sortOrder' => $sortOrder,
                            'validation' => [
                                'required-entry' => true
                            ],
                        ],
                    ],
                ],
            ],
            $options
        );
    }

    /**
     * Get config for "Price" field
     *
     * @param int $sortOrder
     * @return array
     */
    protected function getCityFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('City'),
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_CITY_NAME,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'validation' => [
                            'required-entry' => true
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Get config for "Price Type" field
     *
     * @param int $sortOrder
     * @param array $config
     * @return array
     */
    protected function getCountryFieldConfig($sortOrder, array $config = [])
    {
        return array_replace_recursive(
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __('Country'),
                            'componentType' => Field::NAME,
                            'formElement' => Select::NAME,
                            'dataScope' => static::FIELD_COUNTRY_NAME,
                            'dataType' => Text::NAME,
                            'sortOrder' => $sortOrder,
                            'options' => $this->country->toOptionArray(),
                            'validation' => [
                                'required-entry' => true
                            ],
                        ],
                    ],
                ],
            ],
            $config
        );
    }

    /**
     * Get config for "Required" field
     *
     * @param int $sortOrder
     * @return array
     */
    protected function getRequireFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Required'),
                        'componentType' => Field::NAME,
                        'formElement' => Checkbox::NAME,
                        'dataScope' => static::FIELD_IS_REQUIRE,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'value' => '1',
                        'valueMap' => [
                            'true' => '1',
                            'false' => '0'
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Get config for hidden field used for removing rows
     *
     * @param int $sortOrder
     * @return array
     */
    protected function getIsDeleteFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => ActionDelete::NAME,
                        'fit' => true,
                        'sortOrder' => $sortOrder
                    ],
                ],
            ],
        ];
    }

    /**
     * Get currency symbol
     *
     * @return string
     */
    protected function getCurrencySymbol()
    {
        return $this->storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();
    }

    /**
     * @return bool
     */
    protected function useGroupon()
    {
        $product = $this->locator->getProduct();
        $productId = $product->getId();
        $modelDeal = $this->deal->create()->load($productId, 'product_id');
        $actionName = $this->request->getFullActionName();
        $isGroupon = false;
        if ($modelDeal && $modelDeal->getEnable() == 1) {
            if ($actionName == self::CONTROLLER_ACTION_EDIT_PRODUCT) {
                /** @var \Magento\Catalog\Model\Product $product */
                $product = $this->locator->getProduct();
                if ($product->getTypeId() == self::PRODUCT_TYPE) {
                    $isGroupon = true;
                }
            } elseif ($actionName == self::CONTROLLER_ACTION_NEW_PRODUCT) {
                if (self::PRODUCT_TYPE == $this->request->getParam('type')) {
                    $isGroupon = true;
                }
            }
        }

        return $isGroupon;
    }

    /**
     * @return bool
     */
    protected function isCoupon()
    {
        $actionName = $this->request->getFullActionName();
        $isGroupon = false;
        if ($actionName == 'catalog_product_new') {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->locator->getProduct();
            if ($product->getTypeId() == self::PRODUCT_TYPE_COUPON) {
                $isGroupon = true;
            }
        } elseif ($actionName == 'catalog_product_edit') {
            if (self::PRODUCT_TYPE_COUPON == $this->request->getParam('type')) {
                $isGroupon = true;
            }
        }

        return $isGroupon;
    }
}
