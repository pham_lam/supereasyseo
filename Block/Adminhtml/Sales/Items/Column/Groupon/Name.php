<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05/10/2016
 * Time: 14:05
 */
namespace Magenest\Groupon\Block\Adminhtml\Sales\Items\Column\Groupon;

/**
 * Class Name
 * @package Magenest\Groupon\Block\Adminhtml\Sales\Items\Column\Groupon
 */
class Name extends \Magento\Sales\Block\Adminhtml\Items\Column\Name
{
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\CatalogInventory\Api\StockConfigurationInterface $stockConfiguration,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Model\Product\OptionFactory $optionFactory,
        array $data
    ) {
        parent::__construct($context, $stockRegistry, $stockConfiguration, $registry, $optionFactory, $data);
    }
}
