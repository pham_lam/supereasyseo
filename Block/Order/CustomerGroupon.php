<?php
/**
 * Created by PhpStorm.
 * User: canhnd
 * Date: 19/01/2017
 * Time: 11:04
 */
namespace Magenest\Groupon\Block\Order;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\Session as CustomerSession;
use Magenest\Groupon\Model\GrouponFactory;
use Magenest\Groupon\Model\DealFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Block\Product\AbstractProduct;

/**
 * Class CustomerGroupon
 * @package Magenest\Groupon\Block\Order
 */
class CustomerGroupon extends Template
{

    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'order/customer/groupon.phtml';

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var GrouponFactory
     */
    protected $_grouponFactory;

    /**
     * @var DealFactory
     */
    protected $_dealFactory;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var AbstractProduct
     */
    protected $_absProduct;

    /**
     * CustomerGroupon constructor.
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param GrouponFactory $grouponFactory
     * @param DealFactory $dealFactory
     * @param ProductFactory $productFactory
     * @param AbstractProduct $abstractProduct
     * @param array $data
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        GrouponFactory $grouponFactory,
        DealFactory $dealFactory,
        ProductFactory $productFactory,
        AbstractProduct $abstractProduct,
        array $data = []
    ) {
        $this->_absProduct = $abstractProduct;
        $this->_dealFactory = $dealFactory;
        $this->_productFactory = $productFactory;
        $this->_grouponFactory = $grouponFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('My Tickets'));
    }

    /**
     * Get Customer ID
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->_customerSession->getCustomerId();
    }

    /**
     * Get Groupon
     *
     * @return array
     */
    public function getGroupon()
    {
        $customerId = $this->getCustomerId();
        
        $data = $this->_grouponFactory->create()
            ->getCollection()
            ->addFieldToFilter('customer_id', $customerId)
            ->getData();
        
        return $data;
    }

    /**
     * @param $productId
     * @return mixed
     */
    public function getDealData($productId)
    {
        $model = $this->_dealFactory->create()->load($productId, 'product_id');

        return $model->getGrouponExpire();
    }

    /**
     * @param $product
     * @param $param
     * @return \Magento\Catalog\Block\Product\Image
     */
    public function getProductImage($product, $param)
    {
        $img = $this->_absProduct->getImage($product, $param);

        return $img;
    }

    /**
     * Get Product
     *
     * @param $productId
     * @return $this
     */
    public function getItems($productId)
    {
        $product = $this->_productFactory->create()->load($productId);

        return $product;
    }

    /**
     * @param $grouponId
     * @return string
     */
    public function printUrl($grouponId)
    {
        $url = $this->getUrl('groupon/order/printGroupon', ['groupon_id'=>$grouponId]);
         
        return $url;
    }
}
