<?php
/**
 * Created by PhpStorm.
 * User: canhnd
 * Date: 19/01/2017
 * Time: 14:31
 */
namespace Magenest\Groupon\Block\Product\View;

use Magento\Catalog\Model\Product;

/**
 * Class Location
 * @package Magenest\Groupon\Block\Product\View
 */
class Location extends \Magento\Framework\View\Element\Template
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * @var \Magenest\Groupon\Model\DealFactory
     */
    protected $_dealFatory;

    /**
     * Location constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magenest\Groupon\Model\DealFactory $dealFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magenest\Groupon\Model\DealFactory $dealFactory,
        array $data = []
    ) {
        $this->_dealFatory = $dealFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function construct()
    {
        parent::_construct();
    }

    /**
     * Get Deals
     *
     * @return array
     */
    public function getDeals()
    {
        $collection = $this->_dealFatory->create()
            ->getCollection()
            ->addFieldToFilter('product_id', $this->getProductId())
            ->getData();

        return $collection;
    }

    /**
     * unserialize
     *
     * @param $location
     * @return mixed
     */
    public function getDataLocation($location)
    {

        $data = unserialize($location);
        
        return $data;
    }

    /**
     * Get Product Id
     *
     * @return int
     */
    public function getProductId()
    {

        $productId = $this->getRequest()->getParam('id');

        return $productId;
    }
}
