<?php
/**
 * Created by PhpStorm.
 */
namespace Magenest\Groupon\Block\Product\View;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magenest\Groupon\Model\DealFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockStateInterface;

/**
 * Class Info
 * @package Magenest\Groupon\Block\Product\View
 */
class Info extends \Magento\Framework\View\Element\Template
{
    /**
     * Google Map API key
     */
    const XML_PATH_GOOGLE_MAP_API_KEY = 'groupon/general_config/google_api_key';
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var
     */
    protected $data;

    /**
     * @var DealFactory
     */
    protected $_dealsFactory;

    /**
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var StockStateInterface
     */
    protected $stockStateInterface;

    /**
     * Info constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param OptionFactory $optionFactory
     * @param DealFactory $dealFactory
     * @param OrderFactory $orderFactory
     * @param ProductFactory $productFactory
     * @param StockStateInterface $stockStateInterface
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        DealFactory $dealFactory,
        OrderFactory $orderFactory,
        ProductFactory $productFactory,
        StockStateInterface $stockStateInterface,
        array $data = []
    ) {
        $this->stockStateInterface = $stockStateInterface;
        $this->productFactory = $productFactory;
        $this->_orderFactory = $orderFactory;
        $this->_dealsFactory = $dealFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Framework\App\Config\ScopeConfigInterface
     */
    public function getScopeConfig()
    {
        return $this->_scopeConfig;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->getRequest()->getParam('id');
    }

    /**
     * Get time countdown
     *
     * @return array
     */
    public function getTimeDeal()
    {
        $collection = $this->_dealsFactory->create()
            ->getCollection()
            ->addFieldToFilter('product_id', $this->getProductId())
            ->getData();

        return $collection;
    }

    /**
     * Ajax update option
     *
     * @return string
     */
    public function getUpdateOption()
    {
        return $this->getUrl('groupon/update/option');
    }

    /**
     * Get Product
     */
    public function getProduct()
    {
        return $this->_coreRegistry->registry('product');
    }

    /**
     * Get Number by Product Id
     */
    public function getNumberOrder()
    {
        $data = $this->_orderFactory->create()
            ->getCollection()->getLastItem()->getData();

        return $data;
    }

    /**
     * @return bool
     */
    public function isConfigurableProduct()
    {
        /** @var \Magenest\Groupon\Model\Deal $model */
        $model = $this->_dealsFactory->create()->getCollection()->addFieldToFilter('product_id', $this->getProductId())->getFirstItem();
        if ($model->getDealId() && $model->getProductType() == 'configurable' && $model->getEnable() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function isCouponProduct()
    {
        /** @var \Magenest\Groupon\Model\Deal $model */
        $model = $this->_dealsFactory->create()->getCollection()->addFieldToFilter('product_id', $this->getProductId())->getFirstItem();
        if ($model->getDealId() && $model->getProductType() == 'coupon' && $model->getEnable() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return mixed|null
     */
    public function getLocationCoupon()
    {
        $model = $this->_dealsFactory->create()->load($this->getProductId(), 'product_id');
        $location = unserialize($model->getLocation());
        $data = null;
        if (sizeof($location) > 0) {
            $data = $location;
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getDiscountFormCoupon()
    {
        $modelProduct = $this->productFactory->create()->load($this->getProductId());
        $qtyInStock = $this->stockStateInterface->getStockQty($this->getProductId(), $modelProduct->getStore()->getWebsiteId());
        $price = round($modelProduct->getPrice(), 2);
        $finalPrice = $modelProduct->getFinalPrice();
        $save = round($price - $finalPrice, 2);
        $discount = ($price == 0) ? '0' :  ((round($save/($save+$price), 4))*100).'%';
        $data = [
            'value' => $price,
            'you_save' => $save,
            'discount' => $discount,
            'qty'   => $qtyInStock,
        ];
        return $data;
    }
    /**
     * Symbol of currency
     * @return string
     */
    public function getCurrencySymbol()
    {
        $symbol =  $this->_storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();

        return $symbol;
    }

    /**
     * @return string
     */
    public function getReturnDate()
    {
        return $this->getUrl('groupon/order/date');
    }

    /**
     * @return $this
     */
    public function getLocation($productId)
    {
        $modelDeal = $this->_dealsFactory->create()->load($productId, 'product_id');

        $location = unserialize($modelDeal->getLocation());

        return $location;
    }

    /**
     * @return string
     */
    public function getReturnLocation()
    {
        return $this->getUrl('groupon/update/location', ['_secure' => true]);
    }

    public function getDataLocation()
    {
        return $this->getUrl('groupon/update/formLocation', ['_secure' => true]);
    }
    /**
     * Get Google Map Api key
     *
     * @return mixed
     */
    public function getGoogleApiKey()
    {
        return $this->_scopeConfig->getValue(self::XML_PATH_GOOGLE_MAP_API_KEY);
    }
}
