     define([
    'jquery',
    'mage/url'
], function ($, url) {
    'use strict';

    return function (widget) {

        $.widget('mage.configurable', widget, {
            _configureElement: function (element) {
                var returnValue = this._super(element);

                var optionLocation = document.getElementById("product_groupon_location_id");
                var groupon_location_form = $("#groupon_location_form");
                var optionId = $("input[name=selected_configurable_option]").val();
                var parentProductId = $("input[name=product]").val();
                groupon_location_form.empty();
                $.ajax({
                    type: "GET",
                    url: url.build('groupon/update/formLocation/'),
                    data: {product_id: optionId},
                    showLoader: true,
                    success: function (result) {
                        var temp = " ";
                        if (result.length > 0) {
                            var dataLocation = result;
                            temp += '<select aria-required="true" id="groupon_select_location"  title="" data-selector="groupon_select_location" class="required product-custom-option-location admin__control-select">';
                            temp += '<option>Select Location...</option>';
                            for (var i = 0; i < dataLocation.length; i++) {
                                temp += '<option id="select_location" name = "option_location" ' +
                                    'value="'
                                    + optionId + '_' + dataLocation[i]['record_id']
                                    + '">'
                                    + dataLocation[i]['street'] + ' - ' + dataLocation[i]['city'] + ' - ' + dataLocation[i]['country'];
                                temp += '</option>';
                            }
                            temp += '</select>';
                            groupon_location_form.append(temp);
                            var selectLocation = document.getElementById("groupon_select_location");
                            $(selectLocation).change(function () {
                                var checkValue = $(this).val();
                                var arrayLocation = checkValue.split("_");
                                optionLocation.value = parseFloat(arrayLocation[1]);
                                $(".box-map").empty();
                                getGoogleMap(checkValue);
                            });
                        }
                    },
                    error: function (e) {
                        console.log(e);
                    },
                    timeout: 10000
                });

                $("#qty").show();
                if (!optionId) {
                    $("#value").text('0.00');
                    $("#you_save").text('0.00');
                    $("#discount").text('0.00 %');
                    $("#qty").hide();
                }

                $.ajax({
                    type: "GET",
                    url: url.build('groupon/update/highlight/'),
                    data: {
                        product_id: optionId
                    },
                    showLoader: true,
                    success: function (result) {
                        var term = result.terms;
                        var instruct = result.instruct;

                        $("#terms_groupon").text(term);
                        $("#instructions_groupon").text(instruct);
                    },
                    error: function (e) {
                        console.log(e);
                    },
                    timeout: 10000
                });

                $.ajax({
                    type: "GET",
                    url: url.build('groupon/update/option'),
                    data: {
                        option_id: optionId,
                        parent_product_id: parentProductId
                    },
                    showLoader: true,
                    success: function (result) {
                        var discount = result.discount;
                        var you_save = result.you_save;
                        var qty = result.qty;
                        var value = result.value;

                        $("#value").text(value);
                        $("#you_save").text(you_save);
                        $("#discount").text(discount);
                        $("#qtyValue").text(qty);
                    },
                    error: function (e) {
                        console.log(e);
                    },
                    timeout: 10000
                });

                function getGoogleMap(checkValue)
                {
                    $.ajax({
                        type: "GET",
                        url: url.build('groupon/update/location/'),
                        data: {location_id: checkValue},
                        showLoader: true,
                        success: function (result) {
                            var locationTemplate = " ";
                            var address = result.replace(/\s+/g, '+');
                            var apiKey = url.tempVar;
                            var srcMap = 'https://www.google.com/maps/embed/v1/place?key=' + apiKey + '&q=' + address;
                            locationTemplate += '<div class="box-map">';
                            locationTemplate += '<iframe width="1195" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" ' +
                                'src=' + srcMap +
                                ' allowfullscreen >';
                            locationTemplate += '</iframe>';
                            locationTemplate += '</div>';
                            $(".groupon_map").append(locationTemplate);
                        },
                        error: function (e) {
                            console.log(e);
                        },
                        timeout: 10000
                    });
                }


                return returnValue;
            }
        });

        return $.mage.configurable;
    }
});