var config = {
    paths: {
        "jqueryCountdown": 'Magenest_Groupon/js/jquery.countdown.min'
    },
    shim:{
        'jqueryCountdown':{
            'deps':['jquery']
        }
    },
    config: {
        mixins: {
            'Magento_ConfigurableProduct/js/configurable': {  // Target module
                'Magenest_Groupon/js/configurable': true  // Extender module
            }
        }
    }
};
